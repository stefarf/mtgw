#!/usr/bin/env bash
#DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
docker rm -f mongo
docker run -d --name mongo -v mongo-data:/data/db -p 27017:27017 mongo:4.4

echo
echo "Connection URI:"
echo "mongodb://localhost:27017/?retryWrites=true&loadBalanced=false&serverSelectionTimeoutMS=5000&connectTimeoutMS=10000"