module main

go 1.19

require (
	github.com/labstack/echo/v4 v4.11.1
	github.com/lithammer/shortuuid/v4 v4.0.0
	gitlab.com/stefarf/iferr v0.1.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/google/uuid v1.3.0 // indirect
	github.com/judwhite/go-svc v1.2.1 // indirect
	github.com/labstack/gommon v0.4.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasttemplate v1.2.2 // indirect
	gitlab.com/stefarf/automap v0.1.0 // indirect
	golang.org/x/crypto v0.11.0 // indirect
	golang.org/x/net v0.12.0 // indirect
	golang.org/x/sys v0.10.0 // indirect
	golang.org/x/text v0.11.0 // indirect
)
