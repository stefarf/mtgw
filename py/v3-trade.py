from typing import Any
from typing import Optional

import time
from jax import numpy as jnp
from matplotlib import pyplot as plt

from mtgw import const
from mtgw import helper
from mtgw import mt
from mtgw import norm
from mtgw import types
from mtgw.mt import Order
from mtgw.orders import Orders
from mtgw.predict import predict
from mtgw.trading_env import Market

MAGIC = 3


def get_bars_values(symbol: str, timeframe: int) -> tuple[Any, types.CandlestickBars]:
    values = []
    bars: types.CandlestickBars = []

    for shift in range(const.INPUT_BARS + 1, 0, -1):
        bar = mt.ohlcv(symbol, timeframe, shift)
        dt, o, h, l, c, v = bar["dt"], bar["open"], bar["high"], bar["low"], bar["close"], bar["volume"]

        values.append([o, h, l, c])
        bars.append((dt, o, h, l, c, v))
    return jnp.array(values).reshape(1, -1), bars


class Timeframe1H:
    def __init__(self, symbol: str, market: Market):
        timeframe = const.TF_1H

        bol = mt.ibands(symbol, timeframe, 0)
        bol_lower = bol['lower']
        bol_main = bol['main']
        bol_upper = bol['upper']
        bol_half = (bol_upper - bol_lower) / 2
        self.near_price = bol_half * 0.5

        small = bol_half * 0.25
        self.may_buy = bol_lower + small < market.ask < bol_main + small
        self.may_sell = bol_main - small < market.bid < bol_upper - small

        rsi = mt.rsi(symbol, timeframe, 0)
        rsi_close = rsi['close']
        self.rsi_volatile = rsi_close < 30 or rsi_close > 70
        self.rsi_stable = 35 < rsi_close < 65

        bars_values, start_inp_bars = get_bars_values(symbol, timeframe)

        last_bar = start_inp_bars[-1]
        last_dt, _, _, _, _, _ = last_bar
        print("last dt:", last_dt)

        norm_values, base, scale_min, scale_max = norm.normalize(bars_values)
        self.buy_profit_pts, self.sell_profit_pts = predict(
            norm_inp=norm_values,
            scale_min=scale_min,
            scale_max=scale_max,
            base=base,
            bid=market.bid,
            ask=market.ask,
            point=market.point,
            symbol=symbol,
            start_inp_bars=start_inp_bars)


def calculate_profit(orders: list[Order]) -> float:
    profit = 0.0
    for order in orders:
        profit += order['profit']
    return profit


def get_similar_order(orders_same_op: list[Order],
                      current_price: float,
                      diff_limit: float,
                      point: float,
                      ) -> Optional[Order]:
    for order in orders_same_op:
        diff_price = abs(order['price'] - current_price)
        if diff_price < diff_limit:
            delta = diff_limit - diff_price
            if const.PRINT:
                print(f'Similar order: '
                      f'Diff price = {round(diff_price, 5)} = {round(diff_price / point)} pts, '
                      f'delta = {round(delta / point)} pts')
            return order
    return None


class Trade:

    def __init__(self, symbol: str):
        self.symbol = symbol
        self.market = Market(symbol)
        self.orders = Orders()
        self.tf1h = Timeframe1H(symbol, self.market)

    def run(self) -> None:
        print("1h timeframe summaries:")
        if self.tf1h.may_buy:
            print("- MAY BUY")
        if self.tf1h.may_sell:
            print("- MAY SELL")
        if not self.tf1h.may_buy and not self.tf1h.may_sell:
            print("- NO BUY and NO SELL")
        if self.tf1h.rsi_volatile:
            print("- VOLATILE price")
        if self.tf1h.rsi_stable:
            print("- STABLE price")
        if self.tf1h.buy_profit_pts != 0:
            print(f"- Predicted BUY  profit: {self.tf1h.buy_profit_pts:.1f} pts")
        if self.tf1h.sell_profit_pts != 0:
            print(f"- Predicted SELL profit: {self.tf1h.sell_profit_pts:.1f} pts")

        print("Run trading rules:")

        buy_orders = self.orders.get_same_op_orders(symbol, const.OP_BUY)
        sell_orders = self.orders.get_same_op_orders(symbol, const.OP_SELL)

        if self.tf1h.rsi_volatile:
            if buy_orders or sell_orders:
                print("- VOLATILE price => Immediately close all orders")
                self.close_buy_orders()
                self.close_sell_orders()
            print("- VOLATILE price => Do not proceed / create new order")
            return

        if buy_orders:
            if self.tf1h.sell_profit_pts < const.LIMIT_OPPOSITE_ORDER_PTS:
                print("- Hit the opposite order points => CLOSE BUY ORDERS")
                self.close_buy_orders()

            print("- WAIT for the right time to CLOSE BUY ORDERS")

            profit = calculate_profit(buy_orders)
            print(f"- Floating profit: $ {profit:.2f}")
            if ((self.tf1h.buy_profit_pts <= 0) or
                    (self.tf1h.buy_profit_pts < const.LIMIT_PTS_CLOSE_ORDER and profit > 0)):
                self.close_buy_orders()
                return

            print(f"- KEEP existing BUY ORDERS, predicted profit: {self.tf1h.buy_profit_pts:.1f} pts")
            return

        if sell_orders:
            if self.tf1h.buy_profit_pts < const.LIMIT_OPPOSITE_ORDER_PTS:
                print("- Hit the opposite order points => CLOSE SELL ORDERS")
                self.close_sell_orders()

            print("- WAIT for the right time to CLOSE SELL ORDERS")

            profit = calculate_profit(sell_orders)
            print(f"- Floating profit: $ {profit:.2f}")
            if ((self.tf1h.sell_profit_pts <= 0) or
                    (self.tf1h.sell_profit_pts < const.LIMIT_PTS_CLOSE_ORDER and profit > 0)):
                self.close_sell_orders()
                return

            print(f"- KEEP existing SELL ORDERS, predicted profit: {self.tf1h.sell_profit_pts:.1f} pts")
            return

        if not self.tf1h.rsi_stable:
            print("- Price is not stable yet => NO BUY AND NO SELL")
            return

        if self.tf1h.buy_profit_pts < const.MIN_PTS or self.tf1h.sell_profit_pts < const.MIN_PTS:
            print("- Predicted profit (either BUY or SELL) is below MIN_PTS => NO BUY AND NO SELL")
            return

        if (self.tf1h.may_buy and
                self.tf1h.buy_profit_pts > self.tf1h.sell_profit_pts and
                self.tf1h.buy_profit_pts > const.LIMIT_PTS_OPEN_ORDER):
            self.close_sell_orders()
            self.buy()
            return

        if (self.tf1h.may_sell and
                self.tf1h.sell_profit_pts > self.tf1h.buy_profit_pts and
                self.tf1h.sell_profit_pts > const.LIMIT_PTS_OPEN_ORDER):
            self.close_buy_orders()
            self.sell()
            return

        print("- End of rules => NO BUY AND NO SELL")

    def close_buy_orders(self) -> None:
        # print(f'- Close BUY orders')
        self.orders.close_all_orders(self.symbol, const.OP_BUY, self.market.bid)

    def close_sell_orders(self) -> None:
        # print(f'- Close SELL orders')
        self.orders.close_all_orders(self.symbol, const.OP_SELL, self.market.ask)

    def buy(self) -> None:
        self.open_non_similar_order(const.OP_BUY, self.market.ask)

    def sell(self) -> None:
        self.open_non_similar_order(const.OP_SELL, self.market.bid)

    def open_non_similar_order(self, op: int, price: float) -> None:
        op_name = const.OP_TO_NAME[op]
        print(f'- CHECKING to make new {op_name} order')

        orders_same_op = self.orders.get_same_op_orders(self.symbol, op)
        if not orders_same_op and const.DO_NOT_MAKE_NEW_ORDER:
            print(f'- DO NOT make new {op_name} order')
            return

        limit_factor = (len(orders_same_op) - 1) * const.NEAR_PRICE_MUL_FACTOR + 1 if len(orders_same_op) else 1
        similar = get_similar_order(
            orders_same_op, price,
            self.tf1h.near_price * limit_factor,
            self.market.point)
        if not similar:
            lots_to_order = const.MIN_ORDER_LOTS
            mt.order_send(self.symbol, op, lots_to_order, price, 0, 0, MAGIC)
            print(f'- Send {op_name} order, {lots_to_order} lots, at price {price}')
        else:
            print(f'- Similar {op_name} order found: {similar}')


while True:
    plt.close('all')  # close previous plots

    for symbol in const.CURRENCIES:
        def run_trade_symbol() -> None:
            print(f'[{symbol}]')
            Trade(symbol).run()
            mt.next_loop()
            print()


        helper.try_except(run_trade_symbol)

    if const.PRINT:
        print('=== open orders ===')
        for order in Orders().orders:
            print(order)

    print()
    print(time.asctime())
    print()
    time.sleep(15)
