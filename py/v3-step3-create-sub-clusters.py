import json
import os
import shutil
from typing import Any
from typing import Iterator

from jax import numpy as jnp

from mtgw import clustering
from mtgw import const

for addr_path in os.listdir(const.CLUSTER_DIR):
    addr_dir = os.path.join(const.CLUSTER_DIR, addr_path)

    first_entry_file = os.path.join(addr_dir, "first_entry.json")
    with open(first_entry_file, "r") as f:
        first_entry = json.load(f)
    addr = first_entry["addr"]

    sub_dir = os.path.join(addr_dir, "sub")
    shutil.rmtree(sub_dir, ignore_errors=True)
    print(sub_dir)


    def entry_generator() -> Iterator[clustering.EntryItem]:
        entries_dir = os.path.join(addr_dir, "entries")
        for item_path in os.listdir(entries_dir):
            assert item_path[-5:] == ".json"
            item_file = os.path.join(entries_dir, item_path)
            with open(item_file, "r") as f:
                item = json.load(f)
            yield {"norm_inp_out": jnp.array(item["entry"]), "bar_id": item_path[:-5]}


    def insert_cluster(sub_addr: int, first_entry: Any) -> None:
        filename = os.path.join(sub_dir, f"sub-{sub_addr}/first_entry.json")
        os.makedirs(os.path.dirname(filename))
        with open(filename, 'w') as f:
            json.dump({"addr": addr, "sub_addr": sub_addr, "entry": first_entry.tolist()}, f)


    def insert_entry(sub_addr: int, entry: Any, bar_id: str) -> None:
        filename = os.path.join(sub_dir, f"sub-{sub_addr}/entries/{bar_id}.json")
        os.makedirs(os.path.dirname(filename), exist_ok=True)
        with open(filename, 'w') as f:
            json.dump({"addr": addr, "sub_addr": sub_addr, "entry": entry.tolist()}, f)


    clustering.create_cluster_and_entry(
        entry_generator(), const.SUB_CLUSTER_DISTANCE,
        insert_cluster, insert_entry)
