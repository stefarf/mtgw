import json
import os.path
from typing import Any
from typing import cast
from typing import Iterator

from jax import numpy as jnp
from typing_extensions import TypedDict

from mtgw import const
from mtgw.jaxarray import JaxArray
from mtgw.net_params import Params

PARAMS_DIR = os.path.join(const.DATA_DIR, "params")


class ParamsData(TypedDict):
    size: int
    loss: float
    iteration: int
    avg_inp: list[float]
    avg_out: list[float]
    w1: Any
    b1: Any
    w2: Any
    b2: Any


def params_path_generator() -> Iterator[str]:
    for grp in os.listdir(PARAMS_DIR):
        grp_dir = os.path.join(PARAMS_DIR, grp)
        for addr in os.listdir(grp_dir):
            addr_dir = os.path.join(grp_dir, addr)
            full_path = os.path.join(addr_dir, "params.json")
            yield full_path


def count_params() -> int:
    cnt = 0
    for _ in params_path_generator():
        cnt += 1
    return cnt


def params_generator() -> Iterator[tuple[list[float], list[float], float, Params]]:
    for full_path in params_path_generator():
        with open(full_path, 'r') as f:
            data = cast(ParamsData, json.load(f))

        avg_inp = data["avg_inp"]
        avg_out = data["avg_out"]
        loss = data["loss"]
        params = Params(
            w1=jnp.array(data["w1"]),
            b1=jnp.array(data["b1"]),
            w2=jnp.array(data["w2"]),
            b2=jnp.array(data["b2"]),
        )

        yield avg_inp, avg_out, loss, params


def load_model() -> tuple[JaxArray, list[Params], list[float]]:
    number_of_params = count_params()
    print("number of params:", number_of_params)
    mem_inputs = []
    mem_params = []
    mem_losses: list[float] = []

    print("loading model")
    addr = 0
    for avg_inp, _, loss, params in params_generator():
        mem_inputs.append(avg_inp)
        mem_params.append(params)
        mem_losses.append(loss)

        addr += 1
        if addr % 500 == 0:
            print("addr:", addr)
    assert addr == number_of_params
    print("done addr:", addr)

    mem_inputs_arr = jnp.array(mem_inputs)
    assert mem_inputs_arr.shape == (number_of_params, const.INPUT_BARS)

    return JaxArray(mem_inputs_arr), mem_params, mem_losses
