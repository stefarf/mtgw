DateAndTime = str
Open = float
High = float
Low = float
Close = float
Volume = int

CandlestickBar = tuple[DateAndTime, Open, High, Low, Close, Volume]
CandlestickBars = list[CandlestickBar]

CandlestickX = list[str]
CandlestickY = list[tuple[float, float, float, float, int]]
