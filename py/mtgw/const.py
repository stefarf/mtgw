# Boolean
import os

DO_NOT_MAKE_NEW_ORDER = False
NO_TRADE = False
PRINT = True

CURRENCIES = [
    'AUDCADv', 'AUDCHFv', 'AUDJPYv', 'AUDNZDv', 'AUDUSDv',
    'CHFJPYv',
    'EURAUDv', 'EURCADv', 'EURCHFv', 'EURGBPv', 'EURJPYv', 'EURNZDv', 'EURUSDv',
    'GBPAUDv', 'GBPCADv', 'GBPCHFv', 'GBPJPYv', 'GBPNZDv', 'GBPUSDv',
    'NZDCADv', 'NZDCHFv', 'NZDJPYv', 'NZDUSDv',
    'USDCADv', 'USDCHFv', 'USDJPYv',
]
POINT = {
    'AUDCADv': 1e-5,
    'AUDCHFv': 1e-5,
    'AUDJPYv': 0.001,
    'AUDNZDv': 1e-5,
    'AUDUSDv': 1e-5,
    'CHFJPYv': 0.001,
    'EURAUDv': 1e-5,
    'EURCADv': 1e-5,
    'EURCHFv': 1e-5,
    'EURGBPv': 1e-5,
    'EURJPYv': 0.001,
    'EURNZDv': 1e-5,
    'EURUSDv': 1e-5,
    'GBPAUDv': 1e-5,
    'GBPCADv': 1e-5,
    'GBPCHFv': 1e-5,
    'GBPJPYv': 0.001,
    'GBPNZDv': 1e-5,
    'GBPUSDv': 1e-5,
    'NZDCADv': 1e-5,
    'NZDCHFv': 1e-5,
    'NZDJPYv': 0.001,
    'NZDUSDv': 1e-5,
    'USDCADv': 1e-5,
    'USDCHFv': 1e-5,
    'USDJPYv': 0.001,

}

MIN_ORDER_LOTS = 0.01
MIN_PTS = -50
ORDER_LOTS_FACTOR = MIN_ORDER_LOTS / 0.01

LIMIT_LOSS = -0.8 * ORDER_LOTS_FACTOR
LIMIT_PTS_OPEN_ORDER = 100
LIMIT_PTS_CLOSE_ORDER = 50
LIMIT_OPPOSITE_ORDER_PTS = -100

NEAR_PRICE_MUL_FACTOR = 0.75

# Order operations
OP_BUY = 0
OP_SELL = 1
OP_TO_NAME = {
    OP_BUY: "BUY",
    OP_SELL: "SELL",
}

# Timeframes
TF_1D = 1440
TF_1H = 60

###

# Minimum profit to close orders
CLOSE_ORDERS_MIN_PROFIT = 2 * ORDER_LOTS_FACTOR

# LOSS_LIMIT_CLOSE = -2 * ORDER_LOTS_FACTOR

# If the biggest losing order has loss <= MIN_LOSS_TO_CLOSE, then
# trader will search for matched profitable orders and close them to make the total profit at least
# = CLOSE_ORDERS_MIN_PROFIT
MIN_LOSS_TO_CLOSE = -2 * ORDER_LOTS_FACTOR

# Machine Learning related

# N_SIMILAR_INPUTS = 2
# N_EVAL = 5

# THETA = 1 / 30

INPUT_BARS = 24
OUTPUT_BARS = 5

INPUT_SIZE = INPUT_BARS * 4  # o h l c
OUTPUT_SIZE = OUTPUT_BARS * 4  # o h l c
HIDDEN_SIZE = 200

# MEM_SIZE = 5_000

# LR_BASE = 1e-6
# LR_DECAY = 0.3
# GRAD_THETA = 0.1

# CLUSTER2_DIR = os.path.join(const.DATA_DIR, "cluster2")
# LOSS_LIMIT = 0.02
# LOOP_LIMIT = 100_000
# REMOVE_EXISTING_RESULT_JSON = True

CLUSTER_DISTANCE = 1.7
SUB_CLUSTER_DISTANCE = 0.6  # TODO need to experiment with value between 0.6 and 0.7

DATA_DIR = os.environ["DATA_DIR"] if "DATA_DIR" in os.environ else "D:\\source\\mtgw\\data"

CLUSTER_DIR = "/tmp/mtgw/cluster"

JAX_FILE = os.path.join(DATA_DIR, "jax.pickle")

PLOT_CANDLESTICK = False
