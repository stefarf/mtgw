from typing import Any
from typing import Callable
from typing import Union
from typing import cast

import requests
import time
import yaml
from typing_extensions import TypedDict

from mtgw import const


class Response:
    status_code: int
    text: str


Request = Callable[[], Response]


def _check_error_get_result(r: requests.Response, name: str) -> Any:
    if r.status_code != 200:
        raise RuntimeError(f'{name} error: status code {r.status_code}')
    result = yaml.safe_load(r.text)['result']
    errors = result['errors'] if 'errors' in result else ''
    if errors:
        raise RuntimeError(f'{name} errors: {errors}')
    return result


def chart(symbol: str, timeframe: int) -> None:
    with requests.post(
            'http://localhost/command',
            headers={'Command': 'chart'},
            data=yaml.safe_dump({'symbol': symbol, 'timeframe': timeframe})) as r:
        if r.status_code == 200:
            print(yaml.safe_load(r.text)['result'])


class IBands(TypedDict):
    main: float
    upper: float
    lower: float


def ibands(symbol: str, timeframe: int, shift: int, period: int = 20) -> IBands:
    with requests.post(
            'http://localhost/command',
            headers={'Command': 'iBands'},
            data=yaml.safe_dump({
                'symbol': symbol,
                'timeframe': timeframe,
                'period': period,
                'shift': shift,
            })) as r:
        return cast(IBands, _check_error_get_result(r, 'iBands'))


class MarketInfo(TypedDict):
    bid: float
    ask: float
    point: Union[float, str]


def market_info(symbol: str) -> MarketInfo:
    with requests.post(
            'http://localhost/command',
            headers={'Command': 'marketInfo'},
            data=yaml.safe_dump({'symbol': symbol})) as r:
        return cast(MarketInfo, _check_error_get_result(r, 'marketInfo'))


def next_loop() -> None:
    with requests.post('http://localhost/command', headers={'Command': 'nextLoop'}) as r:
        _check_error_get_result(r, 'nextLoop')


class OHLCV(TypedDict):
    dt: str
    open: float
    high: float
    low: float
    close: float
    volume: int


def ohlcv(symbol: str, timeframe: int, shift: int) -> OHLCV:
    with requests.post(
            'http://localhost/command',
            headers={'Command': 'ohlcv'},
            data=yaml.safe_dump({'symbol': symbol, 'timeframe': timeframe, 'shift': shift})) as r:
        return cast(OHLCV, _check_error_get_result(r, 'ohlcv'))


def order_close(symbol: str, ticket: int, volume: float, price: float) -> None:
    data = yaml.safe_dump({'ticket': ticket, 'volume': volume, 'price': price})
    with open(symbol + '.log', 'a') as f:
        f.write(f'\n{time.asctime()} Send order >>>\n{data}\n<<< end\n')
    with requests.post('http://localhost/command', headers={'Command': 'orderClose'}, data=data) as r:
        result = _check_error_get_result(r, 'orderClose')
        print(result)
        with open(symbol + '.log', 'a') as f:
            f.write(f'\n{time.asctime()} Result >>>\n{result}\n<<< end\n')


class Order(TypedDict):
    ticket: int
    symbol: str
    operation: int
    volume: float
    price: float
    stop_loss: float
    take_profit: float
    profit: float
    magic: int
    open_time: str


class OrderListDict(TypedDict):
    orders: list[Order]


def order_list() -> OrderListDict:
    with requests.post('http://localhost/command', headers={'Command': 'orderList'}) as r:
        rst = _check_error_get_result(r, 'orderList')
        # print('DEBUG:', rst)
        return cast(OrderListDict, rst)


def order_send(symbol: str, operation: int, volume: float,
               price: float, stop_loss: float, take_profit: float,
               magic: int) -> None:
    if const.NO_TRADE:
        print('NO TRADE: Do not send order')
        return
    data = yaml.safe_dump({
        'symbol': symbol,
        'operation': operation,
        'volume': volume,
        'price': price,
        'stop_loss': stop_loss,
        'take_profit': take_profit,
        'magic': magic,
    })
    with open(symbol + '.log', 'a') as f:
        f.write(f'\n{time.asctime()} Send order >>>\n{data}\n<<< end\n')
    with requests.post('http://localhost/command', headers={'Command': 'orderSend'}, data=data) as r:
        result = _check_error_get_result(r, 'orderSend')
        print(result)
        with open(symbol + '.log', 'a') as f:
            f.write(f'\n{time.asctime()} Result >>>\n{result}\n<<< end\n')


class RSI(TypedDict):
    close: float
    typical: float
    weighted: float


def rsi(symbol: str, timeframe: int, shift: int, period: int = 14) -> RSI:
    with requests.post(
            'http://localhost/command',
            headers={'Command': 'rsi'},
            data=yaml.safe_dump({
                'symbol': symbol,
                'timeframe': timeframe,
                'period': period,
                'shift': shift,
            })) as r:
        return cast(RSI, _check_error_get_result(r, 'rsi'))


class Stochastic(TypedDict):
    main: float
    signal: float


def stochastic(symbol: str, timeframe: int, shift: int, k: int = 3, d: int = 2, slowing: int = 2) -> Stochastic:
    with requests.post(
            'http://localhost/command',
            headers={'Command': 'stochastic'},
            data=yaml.safe_dump({
                'symbol': symbol,
                'timeframe': timeframe,
                'k': k,
                'd': d,
                'slowing': slowing,
                'shift': shift,
            })) as r:
        return cast(Stochastic, _check_error_get_result(r, 'stochastic'))
