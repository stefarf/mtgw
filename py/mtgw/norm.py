from typing import Any


def normalize(values: Any, *, scale_min: Any = None, scale_max: Any = None) -> tuple[Any, Any, Any, Any]:
    start = values[:, 0:4]  # first (o, h, l, c)
    base = start[:, 3:4]  # the c price
    val1 = values[:, 4:]  # second and the next (o, h, l, c)
    scaled = (val1 - base) / base

    if scale_min is None:
        scale_min = scaled.min(axis=1).reshape(-1, 1)
    if scale_max is None:
        scale_max = scaled.max(axis=1).reshape(-1, 1)

    norm_values = (scaled - scale_min) / (scale_max - scale_min)
    return norm_values, base, scale_min, scale_max


def denormalize(norm_values: Any, scale_min: Any, scale_max: Any, base: Any) -> Any:
    scaled = norm_values * (scale_max - scale_min) + scale_min
    val1 = scaled * base + base
    return val1
