MAX_ORDER_LOTS = 0.05

# How many digits after decimal/point for lots
ROUND_LOTS = 2

# Number of orders which is considered too many
TOO_MANY_ORDERS = 2
