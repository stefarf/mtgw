import jax.random
from jax import random as jrand


class PRNG:
    def __init__(self, seed: int = 0) -> None:
        self._key = jrand.PRNGKey(seed)

    def key(self) -> jax.random.KeyArray:
        self._key, subkey = jrand.split(self._key)
        return subkey
