from mtgw import mt

_THRESHOLD = 0.6


def _trend_from_to(symbol: str, timeframe: int, shift_from: int, shift_to: int) -> tuple[bool, bool]:
    above = 0.0
    below = 0.0
    for shift in range(shift_from, shift_to + 1):
        bol = mt.ibands(symbol, timeframe, shift)
        bol_main = bol['main']

        bar = mt.ohlcv(symbol, timeframe, shift)
        op = bar['open']
        hi = bar['high']
        lo = bar['low']
        cl = bar['close']

        if op > bol_main:
            above += op - bol_main
        if op < bol_main:
            below += bol_main - op
        if hi > bol_main:
            above += hi - bol_main
        if hi < bol_main:
            below += bol_main - hi
        if lo > bol_main:
            above += lo - bol_main
        if lo < bol_main:
            below += bol_main - lo
        if cl > bol_main:
            above += cl - bol_main
        if cl < bol_main:
            below += bol_main - cl
    total = above + below

    up_trend = above / total > _THRESHOLD if total else False
    down_trend = below / total > _THRESHOLD if total else False

    return up_trend, down_trend


def get_up_down_trend(symbol: str, timeframe: int) -> tuple[bool, bool]:
    up1, down1 = _trend_from_to(symbol, timeframe, 0, 4)
    up2, down2 = _trend_from_to(symbol, timeframe, 0, 1)
    return up1 and up2, down1 and down2
