import time
from typing import Any
from typing import Callable

import yaml

DT_FORMAT = "%Y.%m.%d %H:%M"


def days_diff(dt1: str, dt2: str) -> float:
    secs = abs(time.mktime(time.strptime(dt1, DT_FORMAT)) - time.mktime(time.strptime(dt2, DT_FORMAT)))
    return secs / 3600 / 24


def save_yaml_file(filename: str, value: Any) -> None:
    with  open(filename, 'w') as f:
        yaml.safe_dump(value, f)


def try_except(f: Callable[[], None]) -> None:
    try:
        f()
    except RuntimeError as err:
        print()
        print('#' * 80)
        print('Error:', err)
        print('#' * 80)
        print()
