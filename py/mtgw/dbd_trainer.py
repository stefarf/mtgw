# import math
# from typing import Any
# from typing import cast
#
# import jax
# from jax import jit
# from jax import numpy as jnp
# from jax import random as jrand
# from jax import tree_map
# from jax import value_and_grad
#
# from mtgw import const
# from mtgw import obj
# from mtgw.jaxarray import JaxArray
#
# Params = Any
#
#
# def random_weight(shape: list[int]) -> jax.Array:
#     return jrand.uniform(obj.prng.key(), shape, minval=-0.05, maxval=0.05)
#
#
# def random_bias(shape: list[int]) -> jax.Array:
#     return jrand.uniform(obj.prng.key(), shape, minval=0.05, maxval=0.05)
#
#
# def define_jit_step(loss_fn):
#     def params_fn(param, grad, lr):
#         return param - grad * lr
#
#     def lr_fn(avg, grad, lr):
#         gg = avg * grad
#         lr = jnp.where(gg > 0, lr + const.LR_BASE, lr)
#         lr = jnp.where(gg < 0, lr * (1 - const.LR_DECAY), lr)
#         return lr
#
#     def avg_fn(avg, grad):
#         return avg * (1 - const.GRAD_THETA) + grad * const.GRAD_THETA
#
#     def step(x, y, params, lr, grad_loss_avg):
#         loss, grad_loss = value_and_grad(loss_fn)(params, x, y)
#         params = tree_map(params_fn, params, grad_loss, lr)
#         lr = tree_map(lr_fn, grad_loss_avg, grad_loss, lr)
#         grad_loss_avg = tree_map(avg_fn, grad_loss_avg, grad_loss)
#         return loss, grad_loss, params, lr, grad_loss_avg
#
#     return jit(step)
#
#
# class Trainer:
#     def __init__(self):
#         self.jit_step = None
#
#     def init_params(self) -> Params:
#         raise RuntimeError("TODO")
#
#     def get_loss_fn(self):
#         raise RuntimeError("TODO")
#
#     def train(self,
#               inp: JaxArray,
#               out: JaxArray,
#               size: int,  # size = number of entries
#               *,
#               loop_limit: int,
#               loss_limit: float) -> tuple[int, float, Params]:
#
#         if self.jit_step is None:
#             self.jit_step = define_jit_step(self.get_loss_fn())
#
#         x = inp.arr
#         y = out.arr
#
#         params = self.init_params()
#
#         lr = tree_map(lambda p: jnp.zeros_like(p) + const.LR_BASE, params)
#         grad_loss_avg = tree_map(lambda p: jnp.zeros_like(p), params)
#
#         best_params = None
#         best_loss = None
#
#         iteration = 0
#         for loop in range(loop_limit):
#             iteration = loop + 1
#
#             loss, grad_loss, params, lr, grad_loss_avg = self.jit_step(x, y, params, lr, grad_loss_avg)
#
#             if iteration % 1_000 == 0:
#                 loss = math.sqrt(loss * 2)
#                 print(f"size: {size}, iteration: {iteration}, loss: {loss:.03f}")
#
#                 if best_loss is None or loss < best_loss:
#                     best_loss = float(loss)
#                     best_params = params
#
#                 if best_loss < loss_limit:
#                     return iteration, best_loss, best_params
#
#                 # if size > const.SKIP_SIZE and best_loss > const.SKIP_LOSS:
#                 #     print(">>> skipped")
#                 #     return iteration, best_loss, best_params
#
#         assert iteration != 0
#         assert best_loss is not None
#         assert best_params is not None
#         return iteration, best_loss, cast(Params, best_params)
