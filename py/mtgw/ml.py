from typing import Any

from jax import jit
from jax import numpy as jnp

RUNNING_AVERAGE_MAX_COUNT = 1_000


def calculate_running_average(val: float, avg: float, cnt: int) -> tuple[float, int]:
    avg = (avg * cnt + val) / (cnt + 1)
    cnt += 1
    if cnt > RUNNING_AVERAGE_MAX_COUNT:
        cnt = RUNNING_AVERAGE_MAX_COUNT
    return avg, cnt


@jit
def jit_append_entry(entries: Any, entry: Any) -> Any:
    return jnp.append(entries, entry, axis=0)


@jit
def jit_find_most_similar(a: Any, b: Any) -> tuple[Any, Any]:
    distances = jnp.sqrt(jnp.power(a - b, 2).sum(axis=1))
    addr = distances.argmin()
    distance = distances[addr]
    return addr, distance


@jit
def jit_find_similar_sorted(a: Any, b: Any) -> tuple[Any, Any]:
    distances = jnp.sqrt(jnp.power(a - b, 2).sum(axis=1))
    addr = distances.argsort()
    return addr, distances[addr]
