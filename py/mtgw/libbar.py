from typing import Any

from jax import numpy as jnp

from mtgw import types


def bars_to_tensor(bars: types.CandlestickBars) -> Any:
    values: list[tuple[float, float, float, float]] = []
    for dt, o, h, l, c, v in bars:
        values.append((o, h, l, c))
    return jnp.array(values).reshape(1, -1)
