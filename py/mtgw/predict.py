import pickle
from typing import Any
from typing import Iterator

from mtgw import candlestick
from mtgw import const
from mtgw import ml
from mtgw import norm
from mtgw import types


def _addrs_distances_within_limit_generator(
        addrs: Any,
        distances: Any,
        distance_limit: float,
        *,
        print_more_than_limit: bool = False) -> Iterator[tuple[int, float]]:
    for addr, distance in zip(addrs.tolist(), distances.tolist()):
        if distance > distance_limit:
            if print_more_than_limit and distance < distance_limit * 1.1:
                print(f"=== distance = {distance:.3f} > limit ===")
            break
        yield addr, distance


def _values_to_bars(values: list[tuple[float, float, float, float]]) -> types.CandlestickBars:
    bars: types.CandlestickBars = []
    idx = 1
    for o, h, l, c in values:
        bars.append((str(idx), o, h, l, c, 0))
        idx += 1
    return bars


def _get_bars_highest_lowest(bars: types.CandlestickBars) -> tuple[float, float]:
    high, low = [], []
    for dt, o, h, l, c, v in bars:
        high.append(h)
        low.append(l)
    return max(high), min(low)


print("load from file:", const.JAX_FILE)
with open(const.JAX_FILE, 'rb') as f:
    top_inputs = pickle.load(f)
    sub_map = pickle.load(f)


def predict(*,
            norm_inp: Any,
            scale_min: Any,
            scale_max: Any,
            base: Any,

            bid: float,
            ask: float,
            point: float,

            symbol: str,
            start_inp_bars: types.CandlestickBars,
            ) -> tuple[float, float]:
    norm_preds = []

    addrs, distances = ml.jit_find_similar_sorted(top_inputs, norm_inp)
    for addr, distance in _addrs_distances_within_limit_generator(
            addrs, distances, const.CLUSTER_DISTANCE):
        sub_inputs, sub_outputs = sub_map[addr]

        sub_addrs, sub_distances = ml.jit_find_similar_sorted(sub_inputs, norm_inp)
        for sub_addr, sub_distance in _addrs_distances_within_limit_generator(
                sub_addrs, sub_distances, const.SUB_CLUSTER_DISTANCE, print_more_than_limit=True):
            print("top distance:", distance)
            print("sub distance:", sub_distance)
            norm_pred = sub_outputs[sub_addr]
            norm_preds.append(norm_pred)

    buy_profit_pts = 0.0
    sell_profit_pts = 0.0

    if not norm_preds:
        return buy_profit_pts, sell_profit_pts

    highest_lst = []
    lowest_lst = []
    for norm_pred in norm_preds:
        pred = norm.denormalize(norm_pred, scale_min, scale_max, base)
        pred_bars = _values_to_bars(pred.reshape(-1, 4).tolist())

        if const.PLOT_CANDLESTICK:
            candlestick.plot_bars(f"{symbol} (prediction)", start_inp_bars + pred_bars, reset_dt=True)

        highest, lowest = _get_bars_highest_lowest(pred_bars)
        highest_lst.append(highest)
        lowest_lst.append(lowest)

    min_highest = min(highest_lst)
    # if min_highest > ask:
    buy_profit_pts = (min_highest - ask) / point

    max_lowest = max(lowest_lst)
    # if max_lowest < bid:
    sell_profit_pts = (bid - max_lowest) / point

    return buy_profit_pts, sell_profit_pts
