from typing import Any
from typing import Callable
from typing import Iterator
from typing import TypedDict

from mtgw import ml


def plot(a: Any, b: Any) -> None:
    from matplotlib import pyplot as plt
    plt.plot(a)
    plt.plot(b)
    plt.show()


InsertClusterFn = Callable[[int, Any], None]
InsertEntryFn = Callable[[int, Any, str], None]


class Memory:
    def __init__(self,
                 first_entry: Any,
                 bar_id: str,
                 distance_limit: float,
                 *,
                 insert_cluster_fn: InsertClusterFn,
                 insert_entry_fn: InsertEntryFn,
                 ):
        self.entries = first_entry
        self.distance_limit = distance_limit
        self.size = 1

        self.insert_cluster_fn = insert_cluster_fn
        self.insert_entry_fn = insert_entry_fn

        self.insert_cluster_fn(0, first_entry)
        self.insert_entry_fn(0, first_entry, bar_id)

    def append_if_not_similar(self, entry: Any, bar_id: str) -> None:
        addr, distance = ml.jit_find_most_similar(self.entries, entry)
        distance = float(distance)
        # print("distance:", distance)

        if distance > self.distance_limit:
            # not similar
            self.insert_cluster_fn(self.size, entry)
            self.insert_entry_fn(self.size, entry, bar_id)
            self.entries = ml.jit_append_entry(self.entries, entry)
            self.size += 1
        else:
            # similar
            self.insert_entry_fn(int(addr), entry, bar_id)
            # plot(
            #     entry.flatten(),
            #     self.entries[addr].flatten(),
            # )


class EntryItem(TypedDict):
    norm_inp_out: Any
    bar_id: str


def create_cluster_and_entry(
        entry_gen: Iterator[EntryItem],
        distance_limit: float,
        insert_cluster: InsertClusterFn,
        insert_entry: InsertEntryFn) -> None:
    item = next(entry_gen)
    norm_inp_out = item["norm_inp_out"]
    bar_id = item["bar_id"]
    mem = Memory(norm_inp_out, bar_id, distance_limit,
                 insert_cluster_fn=insert_cluster, insert_entry_fn=insert_entry)

    def print_stats(data_cnt: int, mem_size: int, diff_prev: int) -> int:
        diff = data_cnt - mem_size
        diff_up = diff - diff_prev
        print(f"mem size: {mem_size} data count: {data_cnt} "
              f"ratio: data/mem: {round(data_cnt / mem_size, 2)} mem/data: {round(mem_size / data_cnt, 3)} "
              f"diff: {diff} diff up: {diff_up}")
        diff_prev = diff
        return diff_prev

    data_cnt = 1
    diff_prev = 0
    for item in entry_gen:
        norm_inp_out = item["norm_inp_out"]
        bar_id = item["bar_id"]
        mem.append_if_not_similar(norm_inp_out, bar_id)

        data_cnt += 1
        if data_cnt % 100 == 0:
            diff_prev = print_stats(data_cnt, mem.size, diff_prev)
    print_stats(data_cnt, mem.size, diff_prev)
    print("done")
