from mtgw import mt
from mtgw.trend import get_up_down_trend


class Band:
    def __init__(self, symbol: str, timeframe: int, shift: int = 1):
        band = mt.ibands(symbol, timeframe, shift)
        self.main = band['main']
        self.upper = band['upper']
        self.lower = band['lower']


class Market:
    def __init__(self, symbol: str):
        market = mt.market_info(symbol)
        self.bid = market['bid']
        self.ask = market['ask']
        self.point = float(market['point'])

        # helper.save_yaml_file(f'{symbol}.market.yaml', market)


class OHLCV:
    def __init__(self, symbol: str, timeframe: int, shift: int = 0):
        ohlcv = mt.ohlcv(symbol, timeframe, shift)
        self.close = ohlcv['close']


class Trend:
    def __init__(self, symbol: str, timeframe: int):
        self.up, self.down = get_up_down_trend(symbol, timeframe)
