from copy import deepcopy
from typing import Optional

from mtgw import const, mt
from mtgw.mt import Order

OrdersGroup = dict[str, list[Order]]


def _group_orders(orders: list[Order]) -> OrdersGroup:
    orders_group: OrdersGroup = {}
    for order in orders:
        symbol = order['symbol']
        if symbol not in orders_group:
            orders_group[symbol] = []
        orders_group[symbol].append(order)
    return orders_group


def _separate_positive_negative_orders_group(orders_group: OrdersGroup) -> tuple[OrdersGroup, OrdersGroup]:
    pos_profit_orders_group: OrdersGroup = {}
    neg_profit_orders_group: OrdersGroup = {}
    for symbol, orders in orders_group.items():
        if orders[0]['profit'] >= 0:
            pos_profit_orders_group[symbol] = orders
        else:
            neg_profit_orders_group[symbol] = orders
    return pos_profit_orders_group, neg_profit_orders_group


def _pick_smallest_profit_order(orders_group: OrdersGroup) -> Optional[Order]:
    smallest = None
    for symbol, orders in orders_group.items():
        oldest = orders[0]
        profit = oldest['profit']
        if smallest is None or profit < smallest['profit']:
            smallest = oldest
    if smallest:
        del orders_group[smallest['symbol']]  # so it won't be picked again
    return smallest


def _match_orders(negative_profit: float,
                  pos_profit_orders_group: OrdersGroup,
                  smallest_profit_order: Order,
                  ) -> list[Order]:
    total_profit = negative_profit
    pos_profit_orders_group = deepcopy(pos_profit_orders_group)

    matched = [smallest_profit_order]
    while pos_profit_orders_group:
        profit_orders = []
        deleted = []
        for symbol, orders in pos_profit_orders_group.items():
            oldest = orders.pop(0)
            profit_orders.append(oldest)
            if not orders or orders[0]['profit'] < 0:
                deleted.append(symbol)
        for symbol in deleted:
            del pos_profit_orders_group[symbol]
        if not profit_orders:
            return []
        profit_orders.sort(key=lambda o: o['profit'])

        while profit_orders:
            order = profit_orders.pop()
            matched.append(order)
            profit = order['profit']
            total_profit += profit
            if const.PRINT:
                print(f'Add profit {profit}, total profit {round(total_profit, 2)}')
            if total_profit >= const.CLOSE_ORDERS_MIN_PROFIT:
                return matched

    return []


def find_orders_to_close(orders: list[Order]) -> list[Order]:
    orders.sort(key=lambda o: o['open_time'])

    orders_group = _group_orders(orders)
    pos_profit_orders_group, neg_profit_orders_group = _separate_positive_negative_orders_group(orders_group)

    while neg_profit_orders_group:
        smallest_profit_order = _pick_smallest_profit_order(neg_profit_orders_group)
        if not smallest_profit_order:
            return []

        negative_profit = smallest_profit_order['profit']
        if const.PRINT:
            print('The most loss order:', smallest_profit_order)
            print(f'Total profit {negative_profit}')
        if negative_profit > const.MIN_LOSS_TO_CLOSE:
            if const.PRINT:
                print(f'Total profit > {const.MIN_LOSS_TO_CLOSE}')
            return []

        matched = _match_orders(negative_profit, pos_profit_orders_group, smallest_profit_order)
        if matched:
            return matched

        if const.PRINT:
            print('No matched profit order')

    return []


class Orders:
    def __init__(self) -> None:
        self.orders: list[Order] = []
        self.refresh()

    def calculate_total(self, symbol: str) -> tuple[int, float]:
        total_orders = 0
        total_profit = 0.0
        for order in self.orders:
            if order['symbol'] == symbol:
                total_orders += 1
                total_profit += order['profit']
        return total_orders, round(total_profit, 2)

    def close_all_orders(self, symbol: str, op: int, close_price: float) -> None:
        cnt = 0
        for order in self.orders:
            if order['symbol'] == symbol and order['operation'] == op:
                cnt += 1
                mt.order_close(symbol, order['ticket'], order['volume'], close_price)
        if cnt:
            print(f'- CLOSE {cnt} {const.OP_TO_NAME[op]} ORDER(s)')

    def find_orders_to_close(self) -> list[Order]:
        return find_orders_to_close(self.orders)

    def get_same_op_orders(self, symbol: str, op: int) -> list[Order]:
        orders_same_op = []
        for order in self.orders:
            if order['symbol'] == symbol and order['operation'] == op:
                orders_same_op.append(order)
        return orders_same_op

    def refresh(self) -> None:
        self.orders = mt.order_list()['orders']
        # if not self.orders:
        #     raise RuntimeError('no orders')  # TODO: debug
