from matplotlib import pyplot as plt

from mtgw import types


def _set_labels(xs: list[str], n: int = 20) -> list[str]:
    if len(xs) <= n:
        return xs
    labels = ['' for _ in range(len(xs))]

    steps = (len(xs) - 1) / (n - 1)
    for i in range(n - 1):
        labels[round(i * steps)] = xs[round(i * steps)]
    labels[-1] = xs[-1]
    return labels


def plot(
        title: str,
        xs: list[str], ys: list[tuple[float, float, float, float, int]],
        candle_width: int = 4,
) -> None:
    fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    fig.suptitle(title)
    for x, (o, h, l, c, v) in zip(xs, ys):
        ax1.vlines(x, ymin=l, ymax=h, color='black', linewidth=1)
        if o > c:
            color, ymin, ymax = 'red', c, o
        elif o < c:
            color, ymin, ymax = 'green', o, c
        else:
            color, ymin, ymax = 'black', o, c
        ax1.vlines(x, ymin=ymin, ymax=ymax, color=color, linewidth=candle_width)

    vs = [v for _, _, _, _, v in ys]
    ax2.bar(xs, vs)
    plt.xticks(xs, _set_labels(xs), rotation='vertical')
    plt.show()


def _bars_to_xy(bars: types.CandlestickBars) -> tuple[types.CandlestickX, types.CandlestickY]:
    x: types.CandlestickX = []
    y: types.CandlestickY = []
    for dt, o, h, l, c, v in bars:
        x.append(dt)
        y.append((o, h, l, c, v))
    return x, y


def plot_bars(title: str, bars: types.CandlestickBars, *, reset_dt: bool = False) -> None:
    if reset_dt:
        new_bars: types.CandlestickBars = []
        cnt = 1
        for dt, o, h, l, c, v in bars:
            new_bars.append((str(cnt), o, h, l, c, v))
            cnt += 1
        bars = new_bars
    x, y = _bars_to_xy(bars)
    plot(title, x, y)
