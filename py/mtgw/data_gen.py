import copy
import os
from typing import Iterator
from typing import Union
from typing import cast

from mtgw import const
from mtgw import helper
from mtgw import types

TF1H_DIR = os.path.join(const.DATA_DIR, "1h")
TF1H_SUF = "60.csv"
MAX_DAYS_DIFF = 3 + 1 / 24

Lines = list[str]


def lines_generator(
        currencies: list[str] = const.CURRENCIES,
) -> Iterator[tuple[str, Union[str, tuple[Lines, Lines]]]]:
    for symbol in currencies:
        filename = os.path.join(TF1H_DIR, symbol + TF1H_SUF)
        if not os.path.exists(filename):
            print("can not find", filename)
            continue
        with open(filename) as f:
            yield "SYMBOL", symbol
            lines = f.readlines()
            pos = int(len(lines) * 0.9)
            print(f"{symbol} data has {len(lines)} lines, split training/testing at {pos}")
            training_lines, testing_lines = lines[:pos], lines[pos:]
            yield "LINES", (training_lines, testing_lines)


def line_to_bar(line: str) -> types.CandlestickBar:
    d, t, op, hi, lo, cl, vo = line.strip().split(',')
    bar = f'{d} {t}', float(op), float(hi), float(lo), float(cl), int(vo)
    return bar


def ohlcv_generator(
        currencies: list[str] = const.CURRENCIES,
        *,
        training_data: bool = False,
        testing_data: bool = False,
) -> Iterator[tuple[str, Union[str, types.CandlestickBar]]]:
    for desc, data in lines_generator(currencies):
        if desc == "SYMBOL":
            assert type(data) is str
            yield desc, data
        else:
            assert type(data) is not str
            data = cast(tuple[list[str], list[str]], data)
            training_lines, testing_lines = data
            if training_data:
                for line in training_lines:
                    yield "BAR", line_to_bar(line)
            if testing_data:
                for line in testing_lines:
                    yield "BAR", line_to_bar(line)


def bars_generator(
        ohlcv_gen: Iterator[tuple[str, Union[str, types.CandlestickBar]]],
) -> Iterator[tuple[str, types.CandlestickBars]]:
    symbol = None
    bars: types.CandlestickBars = []
    while True:
        try:
            desc, data = next(ohlcv_gen)
        except StopIteration:
            return

        if desc == "SYMBOL":
            assert type(data) is str
            symbol = data
            print("Symbol:", symbol)
            bars = []
            continue

        if desc == "BAR":
            assert type(data) is not str
            bar = cast(types.CandlestickBar, data)
            bars.append(bar)

            if len(bars) >= 2:
                dt1 = bars[-1][0]
                dt2 = bars[-2][0]
                days_diff = helper.days_diff(dt1, dt2)
                if days_diff > MAX_DAYS_DIFF:
                    print(f"[{symbol}] days diff between {dt1} and {dt2} = {days_diff} > {MAX_DAYS_DIFF}")
                    bars = [bar]
                    continue

            # Why `+ 1` in the `len(bars) == const.INPUT_SIZE + const.OUTPUT_SIZE + 1` below ?
            # - For input normalization:
            #     The input sequence will be normalized and the normalization will reduce the size by 1
            #     (remove the start value).
            #     So to make the input size for training equals const.INPUT_SIZE then it needs to be added by 1.
            # - For output normalization:
            #     To normalize the output sequence it uses the last value in the input sequence as the start value
            #     so the result of output normalization will still be equal to const.OUTPUT_SIZE.
            if len(bars) == const.INPUT_BARS + const.OUTPUT_BARS + 1:
                assert symbol is not None
                yield symbol, copy.deepcopy(bars)
                bars = bars[1:]
            continue

        raise RuntimeError("unknown desc")
