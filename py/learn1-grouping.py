import json
import os.path

from mtgw import const
from mtgw import data_gen


def bar_generator():
    symbol = None
    for desc, data in data_gen.lines_generator():
        if desc == "SYMBOL":
            symbol = data
        elif desc == "LINES":
            training_lines, testing_lines = data
            for line in training_lines + testing_lines:
                bar = data_gen.line_to_bar(line)
                yield symbol, bar


def bars_map_to_lst(m):
    lst = []
    for dt in sorted(m.keys()):
        bar = m[dt]
        lst.append(bar)
    return lst


def bars_lst_to_map(lst):
    m = {}
    for bar in lst:
        dt = bar["dt"]
        m[dt] = bar
    return m


def write_map_to_file_as_lst(m, file):
    os.makedirs(os.path.dirname(file), exist_ok=True)
    with open(file, "w") as f:
        json.dump(bars_map_to_lst(m), f, indent=2)


def read_lst_from_file_as_map(file):
    with open(file, "r") as f:
        return bars_lst_to_map(json.load(f))


file = None
prev_file = None
bars_map = {}

for symbol, bar in bar_generator():
    dt, o, h, l, c, v = bar

    ymd, hm = dt.split(" ")
    yy, mm, dd = ymd.split(".")

    file = os.path.join(const.DATA_DIR, "bars", symbol, "1h", yy, f"{mm}.json")
    if file != prev_file:
        if prev_file is not None and bars_map:
            write_map_to_file_as_lst(bars_map, prev_file)
        if os.path.exists(file):
            bars_map = read_lst_from_file_as_map(file)
        else:
            bars_map = {}
        prev_file = file

    bars_map[dt] = {"dt": dt, "o": o, "h": h, "l": l, "c": c, "v": v}

if file is not None and bars_map:
    write_map_to_file_as_lst(bars_map, file)
