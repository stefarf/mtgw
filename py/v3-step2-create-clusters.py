import json
import os.path
from typing import Any
from typing import Iterator

from mtgw import clustering
from mtgw import const
from mtgw import data_gen
from mtgw import libbar
from mtgw import norm


def clean_bar_id(bar_id: str) -> str:
    bar_id = bar_id.replace(" ", "_")
    bar_id = bar_id.replace(".", "_")
    bar_id = bar_id.replace(":", "_")
    return bar_id


def entry_generator() -> Iterator[clustering.EntryItem]:
    for symbol, bars in data_gen.bars_generator(data_gen.ohlcv_generator(training_data=True)):
        values = libbar.bars_to_tensor(bars)
        assert values.shape == (1, (const.INPUT_BARS + const.OUTPUT_BARS + 1) * 4)
        norm_inp_out, _, _, _ = norm.normalize(values)

        first_bar_dt, _, _, _, _, _ = bars[0]
        bar_id = clean_bar_id(f"{symbol}_{first_bar_dt}")
        yield {"norm_inp_out": norm_inp_out, "bar_id": bar_id}


def insert_cluster(addr: int, first_entry: Any) -> None:
    filename = os.path.join(const.CLUSTER_DIR, f"addr-{addr}/first_entry.json")
    os.makedirs(os.path.dirname(filename))
    with open(filename, 'w') as f:
        json.dump({"addr": addr, "entry": first_entry.tolist()}, f)


def insert_entry(addr: int, entry: Any, bar_id: str) -> None:
    filename = os.path.join(const.CLUSTER_DIR, f"addr-{addr}/entries/{bar_id}.json")
    os.makedirs(os.path.dirname(filename), exist_ok=True)
    with open(filename, 'w') as f:
        json.dump({"addr": addr, "entry": entry.tolist()}, f)


clustering.create_cluster_and_entry(
    entry_generator(), const.CLUSTER_DISTANCE,
    insert_cluster, insert_entry)
