from mtgw import candlestick
from mtgw import data_gen
from mtgw import libbar
from mtgw import norm

gen = data_gen.bars_generator(data_gen.ohlcv_generator(training_data=True))

symbol, bars = next(gen)
values = libbar.bars_to_tensor(bars)
candlestick.plot_bars(symbol, bars)
norm_val, base, scale_min, scale_max = norm.normalize(values)
print("norm size:", norm_val.size)

denorm_val = norm.denormalize(norm_val, scale_min, scale_max, base)
print("values size:", values.size)
print("denorm size:", denorm_val.size)
print("values - denorm", values[:, 4:] - denorm_val)

print("values:", values)
print("norm_val:", norm_val)
print("base:", base)
print("denorm_val:", denorm_val)
