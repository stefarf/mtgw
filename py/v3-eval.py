import pickle
from typing import Any
from typing import Iterator
from typing import Optional

from jax import numpy as jnp
from matplotlib import pyplot as plt
from typing_extensions import TypedDict

from mtgw import const
from mtgw import data_gen
from mtgw import libbar
from mtgw import ml
from mtgw import norm
from mtgw import types


def plot(a: Any, b: Any) -> None:
    plt.plot(a, label="output")
    plt.plot(b, label="prediction")
    plt.legend()
    plt.show()


def get_bars_highest_lowest(bars: types.CandlestickBars) -> tuple[float, float]:
    high, low = [], []
    for dt, o, h, l, c, v in bars:
        high.append(h)
        low.append(l)
    return max(high), min(low)


def values_to_bars(values: list[tuple[float, float, float, float]]) -> types.CandlestickBars:
    bars: types.CandlestickBars = []
    idx = 1
    for o, h, l, c in values:
        bars.append((str(idx), o, h, l, c, 0))
        idx += 1
    return bars


class Trader:
    def __init__(self) -> None:
        self.symbol: Optional[str] = None
        self.point = 0.0
        self.order_buy = False
        self.order_sell = False
        self.order_price = 0.0
        self.order_target = 0.0
        self.total_profit = 0.0
        self.total_orders = 0

    def on_symbol(self, symbol: str) -> None:
        if self.symbol != symbol:
            print("new symbol:", symbol)
            self.symbol = symbol
            self.point = const.POINT[symbol]
            self.order_buy = False
            self.order_sell = False
            self.order_price = 0.0
            self.order_target = 0.0
            self.total_profit = 0.0

    def on_bar(self, bar: types.CandlestickBar) -> None:
        pass

    def close_buy_order(self, price: float) -> None:
        if not self.order_buy:
            return
        profit = price - self.order_price
        point = int(profit / self.point)
        self.total_profit += profit
        total_point = int(self.total_profit / self.point)
        self.total_orders += 1

        print(f">>> CLOSE buy order, "
              f"PROFIT: {point} pts, total PROFIT: {total_point} pts, "
              f"orders: {self.total_orders}")

        self.order_buy = False
        self.order_price = 0.0
        self.order_target = 0.0

    def close_sell_order(self, price: float) -> None:
        if not self.order_sell:
            return
        profit = self.order_price - price
        point = int(profit / self.point)
        self.total_profit += profit
        total_point = int(self.total_profit / self.point)
        self.total_orders += 1

        print(f">>> CLOSE sell order, "
              f"PROFIT: {point} pts, total PROFIT: {total_point} pts, "
              f"orders: {self.total_orders}")

        self.order_sell = False
        self.order_price = 0.0
        self.order_target = 0.0

    def buy(self, price: float, target: float) -> None:
        if self.order_sell:
            self.close_sell_order(price)

        self.order_target = target

        if self.order_buy:
            print(f"UPDATE buy at {self.order_price}, NEW target {target}")
        else:
            print(f"buy at {price}, target {target}")
            self.order_buy = True
            self.order_price = price

    def sell(self, price: float, target: float) -> None:
        if self.order_buy:
            self.close_buy_order(price)

        self.order_target = target

        if self.order_sell:
            print(f"UPDATE sell at {self.order_price}, NEW target {target}")
        else:
            print(f"sell at {price}, target {target}")
            self.order_sell = True
            self.order_price = price


class EvalData(TypedDict):
    base: Any
    scale_min: Any
    scale_max: Any
    norm_inp: Any
    norm_out: Any

    base_and_inp: Any
    last_price: float

    # for candlestick
    symbol: str
    bars: types.CandlestickBars


trader = Trader()


def eval_generator() -> Iterator[EvalData]:
    for symbol, bars_test in data_gen.bars_generator(
            data_gen.ohlcv_generator([
                # 'AUDCADv', 'AUDCHFv', 'AUDJPYv',
                'AUDNZDv',
                # 'AUDUSDv',
                # 'CHFJPYv',
                # 'EURAUDv', 'EURCADv', 'EURCHFv', 'EURGBPv', 'EURJPYv', 'EURNZDv', 'EURUSDv',
                # 'GBPAUDv', 'GBPCADv', 'GBPCHFv', 'GBPJPYv', 'GBPNZDv', 'GBPUSDv',
                # 'NZDCADv', 'NZDCHFv', 'NZDJPYv', 'NZDUSDv',
                # 'USDCADv', 'USDCHFv', 'USDJPYv',
            ], testing_data=True)):
        trader.on_symbol(symbol)

        last_bar = bars_test[-const.OUTPUT_BARS - 1]
        print("#" * 100)
        print("last bar:", last_bar)
        # print("next bar:", bars_test[-const.OUTPUT_BARS])

        last_price = last_bar[4]
        # print("last_price:", last_price)

        trader.on_bar(last_bar)

        # print("bars test:")
        # for bar in bars_test:
        #     print(bar)
        # highest_test, lowest_test = get_bars_highest_lowest(bars_test[-const.OUTPUT_BARS:])

        values_test = libbar.bars_to_tensor(bars_test)
        # candlestick.plot(symbol, x_test, y_test)
        base_and_inp_test = values_test[:, :const.INPUT_SIZE + 4]  # base + input values
        # out_test = values_test[:, -const.OUTPUT_SIZE - 4:]
        # flat_out_test_arr = values_test.flatten()[-const.OUTPUT_SIZE:]

        norm_values_test, base, scale_min, scale_max = norm.normalize(values_test)

        yield {
            "base": base,
            "scale_min": scale_min,
            "scale_max": scale_max,
            "norm_inp": norm_values_test[:, :const.INPUT_SIZE],
            "norm_out": norm_values_test[:, -const.OUTPUT_SIZE:],

            "base_and_inp": base_and_inp_test,
            "last_price": last_price,

            # for candlestick
            "symbol": symbol,
            "bars": bars_test,
        }


print("load from file:", const.JAX_FILE)
with open(const.JAX_FILE, 'rb') as f:
    top_inputs = pickle.load(f)
    sub_map = pickle.load(f)


def addrs_distances_within_limit_generator(
        addrs: Any,
        distances: Any,
        distance_limit: float,
) -> Iterator[tuple[int, float]]:
    for addr, distance in zip(addrs.tolist(), distances.tolist()):
        if distance > distance_limit:
            break
        yield addr, distance


found_cnt = 0

for data in eval_generator():
    # if found_cnt == 100:
    #     sys.exit()

    base = data["base"]
    scale_min = data["scale_min"]
    scale_max = data["scale_max"]
    norm_inp = data["norm_inp"]
    norm_out = data["norm_out"]

    base_and_inp = data["base_and_inp"]
    last_price = data["last_price"]

    symbol = data["symbol"]
    bars = data["bars"]

    norm_preds = []

    addrs, distances = ml.jit_find_similar_sorted(top_inputs, norm_inp)
    for addr, distance in addrs_distances_within_limit_generator(
            addrs, distances, const.CLUSTER_DISTANCE):
        # print("addr:", addr)
        # print("distance:", distance)
        sub_inputs, sub_outputs = sub_map[addr]

        sub_addrs, sub_distances = ml.jit_find_similar_sorted(sub_inputs, norm_inp)
        # print("sub_addrs:", sub_addrs)
        # print("sub_distances:", sub_distances)
        for sub_addr, sub_distance in addrs_distances_within_limit_generator(
                sub_addrs, sub_distances, const.SUB_CLUSTER_DISTANCE * 0.85):
            # print("sub_addr:", sub_addr)
            # print("sub_distance:", sub_distance)

            norm_pred = sub_outputs[sub_addr]
            norm_preds.append(norm_pred)

            # print("norm_out - norm_pred =", norm_out - norm_pred)
            # plot(norm_out.flatten(), norm_pred.flatten())

    if norm_preds:
        found_cnt += 1

        # candlestick.plot_bars(symbol, bars)

        highest_lst = []
        lowest_lst = []
        for norm_pred in norm_preds:
            pred = norm.denormalize(norm_pred, scale_min, scale_max, base)
            pred_values = jnp.append(base_and_inp, pred)
            pred_bars = values_to_bars(pred_values.reshape(-1, 4).tolist())
            # candlestick.plot_bars("prediction", pred_bars)

            highest, lowest = get_bars_highest_lowest(pred_bars[-const.OUTPUT_BARS:])
            highest_lst.append(highest)
            lowest_lst.append(lowest)

        buy_profit_pts = 0.0
        sell_profit_pts = 0.0

        if highest_lst:
            min_highest = min(highest_lst)
            # print()
            # print(f"min_highest : {min_highest:.05f}")
            # print(f"highest_test: {highest_test:.05f}")
            if min_highest > last_price:
                # print("BUY:")
                # print(f"min_highest({min_highest:.05f}) > last_price({last_price:.05f})")
                # print(f"diff: min_highest  - last_price = {min_highest - last_price:.05f}")
                # print(f"diff: highest_test - last_price = {highest_test - last_price:.05f}")
                buy_profit_pts = (min_highest - last_price) / trader.point

        if lowest_lst:
            max_lowest = max(lowest_lst)
            # print()
            # print(f"max_lowest : {max_lowest:.05f}")
            # print(f"lowest_test: {lowest_test:.05f}")
            if max_lowest < last_price:
                # print("SELL")
                # print(f"max_lowest({max_lowest:.05f}) < last_price({last_price:.05f})")
                # print(f"diff: last_price - max_lowest  = {last_price - max_lowest:.05f}")
                # print(f"diff: last_price - lowest_test = {last_price - lowest_test:.05f}")
                sell_profit_pts = (last_price - max_lowest) / trader.point

        print(f"Buy profit: {buy_profit_pts:.1f} pts")
        print(f"Sell profit: {sell_profit_pts:.1f} pts")

        if buy_profit_pts > sell_profit_pts and buy_profit_pts > 200:
            # print()
            # print("BUY > SELL")
            trader.buy(last_price, min_highest)
            continue

        if buy_profit_pts < sell_profit_pts and sell_profit_pts > 200:
            # print()
            # print("BUY < SELL")
            trader.sell(last_price, max_lowest)
            continue

    print("NO BUY AND NO SELL")
    trader.close_buy_order(last_price)  # TODO: should close when profit ?
    trader.close_sell_order(last_price)  # TODO: should close when profit ?

print("DONE")
