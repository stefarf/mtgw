import json
import os
import pickle
from typing import Any
from typing import cast
from typing import Iterator

from jax import numpy as jnp
from typing_extensions import TypedDict

from mtgw import const

IOMap = dict[int, list[float]]
SubMap = dict[int, tuple[Any, Any]]


class Item(TypedDict):
    addr_dir: str
    addr: int
    norm_inp: list[float]


class SubItem(TypedDict):
    sub_addr: int
    entry: Any


class SubEntry(TypedDict):
    entry: Any


def create_io_list_from_map(io_map: IOMap, *, is_print: bool = False) -> Any:
    io_lst = []
    addr = 0
    while True:
        if addr not in io_map:
            break
        io_lst.append(io_map[addr])
        addr += 1
        if addr % 1000 == 0:
            if is_print:
                print("addr:", addr)
    if is_print:
        print("addr:", addr)
    size = addr
    return jnp.array(io_lst).reshape(size, -1)[:, :const.INPUT_SIZE]


def top_item_generator() -> Iterator[Item]:
    for addr_path in os.listdir(const.CLUSTER_DIR):
        addr_dir = os.path.join(const.CLUSTER_DIR, addr_path)
        first_entry_file = os.path.join(addr_dir, "first_entry.json")
        with open(first_entry_file, "r") as f:
            item = json.load(f)
        addr = item["addr"]
        entry = item["entry"]
        assert len(entry) == 1
        norm_inp = entry[0][:const.INPUT_SIZE]

        yield {
            "addr_dir": addr_dir,
            "addr": addr,
            "norm_inp": norm_inp,
        }


def create_top_inputs() -> Any:
    print("creating top inputs ...")
    input_map = IOMap()
    for item in top_item_generator():
        addr = item["addr"]
        norm_inp = item["norm_inp"]
        input_map[addr] = norm_inp

    return create_io_list_from_map(input_map, is_print=True)


def create_sub_map() -> SubMap:
    print("creating sub map ...")
    sub_map = {}

    cnt = 0
    for item in top_item_generator():
        addr = item["addr"]
        addr_dir = item["addr_dir"]

        input_map = dict[int, Any]()
        output_map = dict[int, Any]()

        sub_dir = os.path.join(addr_dir, "sub")
        for sub_addr_path in os.listdir(sub_dir):
            sub_addr_dir = os.path.join(sub_dir, sub_addr_path)

            first_entry_file = os.path.join(sub_addr_dir, "first_entry.json")
            with open(first_entry_file, "r") as f:
                sub_item = cast(SubItem, json.load(f))
            sub_addr = sub_item["sub_addr"]
            entry = sub_item["entry"]
            assert len(entry) == 1
            norm_inp = entry[0][:const.INPUT_SIZE]

            norm_outs = []
            entries_dir = os.path.join(sub_addr_dir, "entries")
            for entry_path in os.listdir(entries_dir):
                entry_file = os.path.join(entries_dir, entry_path)
                with open(entry_file, "r") as f:
                    sub_entry = cast(SubEntry, json.load(f))
                entry = sub_entry["entry"]
                assert len(entry) == 1
                norm_out = entry[0][-const.OUTPUT_SIZE:]
                norm_outs.append(norm_out)
            avg_norm_outs = jnp.array(norm_outs).mean(axis=0)

            input_map[sub_addr] = norm_inp
            output_map[sub_addr] = avg_norm_outs

        sub_inputs = create_io_list_from_map(input_map)
        sub_outputs = create_io_list_from_map(output_map)

        sub_map[addr] = (sub_inputs, sub_outputs)

        cnt += 1
        if cnt % 500 == 0:
            print("cnt:", cnt)

    print("cnt:", cnt)
    return sub_map


# top_inputs: [size, input_size]
# sub_map{
#     addr: {
#         sub_mem: [size, input_size]
#         sub_outputs: [size, output_size]
#     }
# }

top_inputs = create_top_inputs()
sub_map = create_sub_map()

print("dump to file:", const.JAX_FILE)
with open(const.JAX_FILE, 'wb') as f:
    pickle.dump(top_inputs, f)
    pickle.dump(sub_map, f)
