package main

import (
	"runtime"
	"time"

	"github.com/judwhite/go-svc"
	"gitlab.com/stefarf/iferr"

	"main/go/server"
)

func main() {
	go func() {
		for {
			time.Sleep(time.Second * 10)
			runtime.GC()
		}
	}()
	iferr.Fatal(svc.Run(server.New()))
}
