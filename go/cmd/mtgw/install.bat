@echo off
rem run this script as admin

if not exist mtgw.exe (
    echo Build the mtgw before installing by running "go build"
    goto :exit
)

sc create mtgw binpath= "%CD%\mtgw.exe" start= auto DisplayName= "mtgw"
sc description mtgw "mtgw"
net start mtgw
sc query mtgw

echo Check mtgw.log

:exit