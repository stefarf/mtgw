package server

import (
	"errors"
	"net/http"

	"gopkg.in/yaml.v3"
)

type (
	MarketInfoParams struct {
		Symbol string
	}
)

type MarketInfoCommand struct{}

func (i MarketInfoCommand) SetHeader(params any, header http.Header) error {
	p, ok := params.(MarketInfoParams)
	if !ok {
		return errors.New("invalid MarketInfoParams")
	}
	header.Set("Symbol", p.Symbol)
	return nil
}

func (i MarketInfoCommand) UnmarshalParams(b []byte) (any, error) {
	var params MarketInfoParams
	err := yaml.Unmarshal(b, &params)
	if err != nil {
		return nil, err
	}
	if params.Symbol == "" {
		return nil, errors.New("invalid MarketInfoParams")
	}
	return params, nil
}

func (i MarketInfoCommand) UnmarshalResult(b []byte) (any, error) {
	var result struct {
		Bid, Ask, Point float64
		Digits, Spread  int
		Errors          []string
	}
	err := yaml.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
