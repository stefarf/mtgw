package server

import (
	"errors"
	"net/http"

	"gopkg.in/yaml.v3"

	"main/go/helper"
)

type (
	IBandsParams struct {
		Symbol    string
		Timeframe int
		Period    int
		Shift     int
	}
)

type IBandsCommand struct{}

func (i IBandsCommand) SetHeader(params any, header http.Header) error {
	p, ok := params.(IBandsParams)
	if !ok {
		return errors.New("invalid IBandsParams")
	}
	header.Set("Symbol", p.Symbol)
	header.Set("Timeframe", helper.IntToString(p.Timeframe))
	header.Set("Period", helper.IntToString(p.Period))
	header.Set("Shift", helper.IntToString(p.Shift))
	return nil
}

func (i IBandsCommand) UnmarshalParams(b []byte) (any, error) {
	var params IBandsParams
	err := yaml.Unmarshal(b, &params)
	if err != nil {
		return nil, err
	}
	if params.Symbol == "" || params.Timeframe == 0 || params.Period == 0 {
		return nil, errors.New("invalid IBandsParams")
	}
	return params, nil
}

func (i IBandsCommand) UnmarshalResult(b []byte) (any, error) {
	var result struct {
		Main, Upper, Lower float64
		Errors             []string
	}
	err := yaml.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
