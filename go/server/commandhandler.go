package server

import (
	"net/http"
)

type CommandHandler interface {
	SetHeader(params any, header http.Header) error
	UnmarshalParams(b []byte) (any, error)
	UnmarshalResult(b []byte) (any, error)
}

type Command = string

var commandMap = map[Command]CommandHandler{
	"chart":      ChartCommand{},
	"iBands":     IBandsCommand{},
	"marketInfo": MarketInfoCommand{},
	"nextLoop":   NextLoopCommand{},
	"ohlcv":      OhlcvCommand{},
	"orderClose": OrderCloseCommand{},
	"orderList":  OrderListCommand{},
	"orderSend":  OrderSendCommand{},
	"rsi":        RSICommand{},
	"stochastic": StochasticCommand{},
}
