package server

import (
	"errors"
	"net/http"

	"gopkg.in/yaml.v3"

	"main/go/helper"
)

type (
	OrderSendParams struct {
		Symbol        string
		Operation     int
		Volume, Price float64
		StopLoss      float64 `yaml:"stop_loss"`
		TakeProfit    float64 `yaml:"take_profit"`
		Magic         int
	}
)

type OrderSendCommand struct{}

var validOperation = map[int]bool{
	0: true, // buy
	1: true, // sell
	2: true, // buy limit
	3: true, // sell limit
	4: true, // buy stop
	5: true, // sell stop
}

func (i OrderSendCommand) SetHeader(params any, header http.Header) error {
	p, ok := params.(OrderSendParams)
	if !ok {
		return errors.New("invalid OrderSendParams")
	}
	header.Set("Symbol", p.Symbol)
	header.Set("Operation", helper.IntToString(p.Operation))
	header.Set("Volume", helper.Float64ToString(p.Volume))
	header.Set("Price", helper.Float64ToString(p.Price))
	header.Set("Stop-Loss", helper.Float64ToString(p.StopLoss))
	header.Set("Take-Profit", helper.Float64ToString(p.TakeProfit))
	header.Set("Magic", helper.IntToString(p.Magic))
	return nil
}

func (i OrderSendCommand) UnmarshalParams(b []byte) (any, error) {
	var params OrderSendParams
	err := yaml.Unmarshal(b, &params)
	if err != nil {
		return nil, err
	}
	if params.Symbol == "" || !validOperation[params.Operation] || params.Volume == 0 || params.Price == 0 {
		return nil, errors.New("invalid OrderSendParams")
	}
	return params, nil
}

func (i OrderSendCommand) UnmarshalResult(b []byte) (any, error) {
	var result struct {
		Ticket int
		Errors []string
	}
	err := yaml.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
