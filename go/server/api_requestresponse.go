package server

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"

	"main/go/server/responsemap"
	"main/go/server/types"
)

var chMtRequest = make(chan types.MtRequest)

func mtRequestApi(c echo.Context) error {
	log.Println("[MT Request] Start")
	select {
	case req := <-chMtRequest:
		log.Println("[MT Request] Got request:", req)
		h, ok := commandMap[req.Command]
		if !ok {
			return fmt.Errorf("invalid command: %s", req.Command)
		}
		err := h.SetHeader(req.Params, c.Response().Header())
		if err != nil {
			return err
		}

		c.Response().Header().Set("Id", req.Id)
		c.Response().Header().Set("Command", req.Command)

		log.Println("[MT Request] Done")
		return c.String(http.StatusOK, "ok")
	case <-time.After(time.Millisecond * 750):
		log.Println("[MT Request] Timeout")
		return c.String(http.StatusInternalServerError, "error")
	}
}

func mtResponseApi(c echo.Context) error {
	log.Println("[MT Response] Start")
	id := c.Request().Header.Get("Id")
	cmd := c.Request().Header.Get("Command")

	h, ok := commandMap[cmd]
	if !ok {
		return fmt.Errorf("invalid command: %s", cmd)
	}

	b, err := io.ReadAll(c.Request().Body)
	if err != nil {
		return err
	}
	log.Printf("Response command:%s >>> yaml\n%s\n<<< done\n", cmd, string(b))
	result, err := h.UnmarshalResult(b)
	if err != nil {
		return err
	}

	req, ok := responsemap.Get(id)
	if !ok {
		return c.String(http.StatusInternalServerError, "id not found")
	}

	res := types.MtResponse{Command: cmd, Result: result}
	log.Println("[MT Response] Send response:", res)
	select {
	case req.ChRes <- res:
		log.Println("[MT Response] Done")
		return c.String(http.StatusOK, "ok")
	case <-time.After(time.Millisecond * 750):
		log.Println("[MT Response] Timeout")
		return c.String(http.StatusInternalServerError, "timeout")
	}
}
