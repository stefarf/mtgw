package server

import (
	"context"
	"log"
	"net/http"
	"time"

	"github.com/judwhite/go-svc"
	"github.com/labstack/echo/v4"
	"gitlab.com/stefarf/iferr"

	"main/go/server/responsemap"
)

type Server struct {
	e *echo.Echo
}

func New() Server {
	e := echo.New()
	e.HideBanner = true
	e.HTTPErrorHandler = func(err error, c echo.Context) {
		iferr.Print(c.String(http.StatusInternalServerError, err.Error()))
	}

	e.GET("/mt/request", mtRequestApi)
	e.POST("/mt/response", mtResponseApi)

	e.POST("/command", commandApi)

	go func() {
		for {
			time.Sleep(time.Second * 5)
			responsemap.RemoveExpired()
		}
	}()

	return Server{e}
}

func (s Server) Init(env svc.Environment) error {
	log.Printf("is win service? %v\n", env.IsWindowsService())
	return nil
}

func (s Server) Start() error {
	log.Println("Starting ...")
	go func() { iferr.Fatal(s.e.Start(":80")) }()
	log.Println("Started")
	return nil
}

func (s Server) Stop() error {
	log.Println("Stopping ...")
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	return s.e.Shutdown(ctx)
}
