package server

import (
	"errors"
	"net/http"

	"gopkg.in/yaml.v3"

	"main/go/helper"
)

type (
	StochasticParams struct {
		Symbol    string
		Timeframe int
		K         int
		D         int
		Slowing   int
		Shift     int
	}
)

type StochasticCommand struct{}

func (i StochasticCommand) SetHeader(params any, header http.Header) error {
	p, ok := params.(StochasticParams)
	if !ok {
		return errors.New("invalid StochasticParams")
	}
	header.Set("Symbol", p.Symbol)
	header.Set("Timeframe", helper.IntToString(p.Timeframe))
	header.Set("K", helper.IntToString(p.K))
	header.Set("D", helper.IntToString(p.D))
	header.Set("Slowing", helper.IntToString(p.Slowing))
	header.Set("Shift", helper.IntToString(p.Shift))
	return nil
}

func (i StochasticCommand) UnmarshalParams(b []byte) (any, error) {
	var params StochasticParams
	err := yaml.Unmarshal(b, &params)
	if err != nil {
		return nil, err
	}
	if params.Symbol == "" || params.Timeframe == 0 || params.K == 0 || params.D == 0 || params.Slowing == 0 {
		return nil, errors.New("invalid StochasticParams")
	}
	return params, nil
}

func (i StochasticCommand) UnmarshalResult(b []byte) (any, error) {
	var result struct {
		Main   float64
		Signal float64
		Errors []string
	}
	err := yaml.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
