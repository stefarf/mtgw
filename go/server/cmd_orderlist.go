package server

import (
	"net/http"

	"gopkg.in/yaml.v3"
)

type OrderListCommand struct{}

func (i OrderListCommand) SetHeader(params any, header http.Header) error { return nil }
func (i OrderListCommand) UnmarshalParams(b []byte) (any, error)          { return nil, nil }

func (i OrderListCommand) UnmarshalResult(b []byte) (any, error) {
	var result struct {
		Orders []struct {
			Ticket     int
			Symbol     string
			Operation  int
			Volume     float64
			Price      float64
			StopLoss   float64 `yaml:"stop_loss"`
			TakeProfit float64 `yaml:"take_profit"`
			Profit     float64
			Magic      int
			OpenTime   string `yaml:"open_time"`
		}
		Errors []string
	}
	err := yaml.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
