package server

import (
	"errors"
	"net/http"

	"gopkg.in/yaml.v3"

	"main/go/helper"
)

type (
	ChartParams struct {
		Symbol    string
		Timeframe int
	}
)

type ChartCommand struct{}

func (i ChartCommand) SetHeader(params any, header http.Header) error {
	p, ok := params.(ChartParams)
	if !ok {
		return errors.New("invalid ChartParams")
	}
	header.Set("Symbol", p.Symbol)
	header.Set("Timeframe", helper.IntToString(p.Timeframe))
	return nil
}

func (i ChartCommand) UnmarshalParams(b []byte) (any, error) {
	var params ChartParams
	err := yaml.Unmarshal(b, &params)
	if err != nil {
		return nil, err
	}
	if params.Symbol == "" || params.Timeframe == 0 {
		return nil, errors.New("invalid ChartParams")
	}
	return params, nil
}

func (i ChartCommand) UnmarshalResult(b []byte) (any, error) { return nil, nil }
