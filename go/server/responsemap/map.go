package responsemap

import (
	"log"
	"sync"
	"time"

	"gitlab.com/stefarf/automap"

	"main/go/server/types"
)

var mut sync.Mutex
var responseMap = automap.New[string, types.MtRequest](0.5)

func NewRequest(id, cmd string, params any) types.MtRequest {
	return types.MtRequest{
		Id:      id,
		Command: cmd,
		Params:  params,
		Expire:  time.Now().Add(time.Second * 7),
		ChRes:   make(chan types.MtResponse),
	}
}

func Set(req types.MtRequest) {
	mut.Lock()
	defer mut.Unlock()
	responseMap.Set(req.Id, req)
}

func Get(id string) (types.MtRequest, bool) {
	mut.Lock()
	defer mut.Unlock()
	return responseMap.Get(id)
}

func Delete(id string) {
	mut.Lock()
	defer mut.Unlock()
	responseMap.Delete(id)
}

func RemoveExpired() {
	mut.Lock()
	defer mut.Unlock()
	responseMap.ForEach(func(k string, v types.MtRequest) (done bool) {
		if v.Expire.Before(time.Now()) {
			responseMap.Delete(k)
		}
		return false
	})
	if responseMap.Size() != 0 {
		log.Println("Response map size:", responseMap.Size())
	}
}
