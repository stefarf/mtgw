package types

import (
	"time"
)

type MtRequest struct {
	Id      string
	Command string
	Params  any
	Expire  time.Time
	ChRes   chan MtResponse
}

type MtResponse struct {
	Command string
	Result  any
}
