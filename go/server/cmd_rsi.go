package server

import (
	"errors"
	"net/http"

	"gopkg.in/yaml.v3"

	"main/go/helper"
)

type (
	RSIParams struct {
		Symbol    string
		Timeframe int
		Period    int
		Shift     int
	}
)

type RSICommand struct{}

func (i RSICommand) SetHeader(params any, header http.Header) error {
	p, ok := params.(RSIParams)
	if !ok {
		return errors.New("invalid RSIParams")
	}
	header.Set("Symbol", p.Symbol)
	header.Set("Timeframe", helper.IntToString(p.Timeframe))
	header.Set("Period", helper.IntToString(p.Period))
	header.Set("Shift", helper.IntToString(p.Shift))
	return nil
}

func (i RSICommand) UnmarshalParams(b []byte) (any, error) {
	var params RSIParams
	err := yaml.Unmarshal(b, &params)
	if err != nil {
		return nil, err
	}
	if params.Symbol == "" || params.Timeframe == 0 || params.Period == 0 {
		return nil, errors.New("invalid RSIParams")
	}
	return params, nil
}

func (i RSICommand) UnmarshalResult(b []byte) (any, error) {
	var result struct {
		Close    float64
		Typical  float64
		Weighted float64
		Errors   []string
	}
	err := yaml.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
