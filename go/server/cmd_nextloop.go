package server

import (
	"net/http"
)

type (
	NextLoopParams  struct{}
	NextLoopCommand struct{}
)

func (i NextLoopCommand) SetHeader(params any, header http.Header) error { return nil }
func (i NextLoopCommand) UnmarshalParams(b []byte) (any, error)          { return NextLoopParams{}, nil }
func (i NextLoopCommand) UnmarshalResult(b []byte) (any, error)          { return struct{}{}, nil }
