package server

import (
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/lithammer/shortuuid/v4"

	"main/go/helper"
	"main/go/server/responsemap"
)

func commandApi(c echo.Context) error {
	log.Println("[Command] Start")
	cmd := c.Request().Header.Get("Command")

	h, ok := commandMap[cmd]
	if !ok {
		return fmt.Errorf("invalid command: %s", cmd)
	}

	b, err := io.ReadAll(c.Request().Body)
	if err != nil {
		return err
	}

	params, err := h.UnmarshalParams(b)
	if err != nil {
		return err
	}

	id := shortuuid.New()

	req := responsemap.NewRequest(id, cmd, params)
	responsemap.Set(req)

	log.Println("[Command] Send request:", req)
	select {
	case chMtRequest <- req:
	case <-time.After(time.Minute):
		log.Println("[Command] Send request timeout")
		return errors.New("timeout")
	}

	select {
	case res := <-req.ChRes:
		log.Println("[Command] Got response:", res)
		if res.Command != cmd {
			return errors.New("response has invalid id or command")
		}
		c.Response().Header().Set("Content-Type", "application/yaml")

		defer func() {
			responsemap.Delete(id)
		}()

		log.Println("[Command] Done")
		return c.String(http.StatusOK, string(helper.MustYamlMarshal(res)))
	case <-time.After(time.Second * 5):
		log.Println("[Command] Get response timeout")
		return errors.New("timeout")
	}
}
