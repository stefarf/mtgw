package server

import (
	"errors"
	"net/http"

	"gopkg.in/yaml.v3"

	"main/go/helper"
)

type (
	OrderCloseParams struct {
		Ticket        int
		Volume, Price float64
	}
)

type OrderCloseCommand struct{}

func (i OrderCloseCommand) SetHeader(params any, header http.Header) error {
	p, ok := params.(OrderCloseParams)
	if !ok {
		return errors.New("invalid OrderCloseParams")
	}
	header.Set("Ticket", helper.IntToString(p.Ticket))
	header.Set("Volume", helper.Float64ToString(p.Volume))
	header.Set("Price", helper.Float64ToString(p.Price))
	return nil
}

func (i OrderCloseCommand) UnmarshalParams(b []byte) (any, error) {
	var params OrderCloseParams
	err := yaml.Unmarshal(b, &params)
	if err != nil {
		return nil, err
	}
	if params.Ticket == 0 || params.Volume == 0 || params.Price == 0 {
		return nil, errors.New("invalid OrderCloseParams")
	}
	return params, nil
}

func (i OrderCloseCommand) UnmarshalResult(b []byte) (any, error) {
	var result struct {
		Status string
		Errors []string
	}
	err := yaml.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
