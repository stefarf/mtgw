package server

import (
	"errors"
	"net/http"

	"gopkg.in/yaml.v3"

	"main/go/helper"
)

type (
	OhlcvParams struct {
		Symbol    string
		Timeframe int
		Shift     int
	}
)

type OhlcvCommand struct{}

func (i OhlcvCommand) SetHeader(params any, header http.Header) error {
	p, ok := params.(OhlcvParams)
	if !ok {
		return errors.New("invalid OhlcvParams")
	}
	header.Set("Symbol", p.Symbol)
	header.Set("Timeframe", helper.IntToString(p.Timeframe))
	header.Set("Shift", helper.IntToString(p.Shift))
	return nil
}

func (i OhlcvCommand) UnmarshalParams(b []byte) (any, error) {
	var params OhlcvParams
	err := yaml.Unmarshal(b, &params)
	if err != nil {
		return nil, err
	}
	if params.Symbol == "" || params.Timeframe == 0 {
		return nil, errors.New("invalid OhlcvParams")
	}
	return params, nil
}

func (i OhlcvCommand) UnmarshalResult(b []byte) (any, error) {
	var result struct {
		Dt                     string
		Open, High, Low, Close float64
		Volume                 int64
		Errors                 []string
	}
	err := yaml.Unmarshal(b, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
