package helper

import (
	"strconv"

	"gitlab.com/stefarf/iferr"
)

func Float64ToString(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}

func IntToString(i int) string {
	return strconv.FormatInt(int64(i), 10)
}

func MustParseInt(s string) int {
	i, err := strconv.ParseInt(s, 10, 32)
	iferr.Panic(err)
	return int(i)
}
