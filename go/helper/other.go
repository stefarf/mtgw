package helper

import (
	"os"

	"gitlab.com/stefarf/iferr"
)

func MustGetHostname() string {
	name, err := os.Hostname()
	iferr.Panic(err)
	return name
}
