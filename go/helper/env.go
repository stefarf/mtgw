package helper

import (
	"fmt"
	"os"
)

func GetEnvWithDefault(env, defaultValue string) string {
	val := os.Getenv(env)
	if val != "" {
		return val
	}
	return defaultValue
}

func MustGetEnv(key string) string {
	val := os.Getenv(key)
	if val == "" {
		panic(fmt.Sprintf("unknown env: %s", key))
	}
	return val
}
