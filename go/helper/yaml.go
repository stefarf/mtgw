package helper

import (
	"gitlab.com/stefarf/iferr"
	"gopkg.in/yaml.v3"
)

func MustYamlMarshal(v any) []byte {
	b, err := yaml.Marshal(v)
	iferr.Panic(err)
	return b
}
